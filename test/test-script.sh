#!/bin/bash
#@author: Francisco Jose Lozano Serrano

#Check that the mytar program is located in the current working directory and it is a actual
#executable file. If the check fails, print an error message to the standard output and exit
if ! [ -x  mytar ]; then
	echo "mytar is not located in the current working directory or it doesn't have executable permissions"
	exit 1
fi

#Check if a directory named tmp is found in the current working directory. If so, remove
#the directory and its contents recursively
if [ -d tmp ]; then
	rm -r tmp;
fi

#Create a temporary directory named tmp in the current working directory.
mkdir tmp;

#Create some test files
cd tmp
echo "Hello world!" > file1.txt
head /etc/passwd > file2.txt
head -c 2024 /dev/urandom > file3.txt

# Invoke the mytar program so as to create a filetar.mtar archive including the contents
#of the three aforementioned files.
../mytar -cf filetar.mtar file1.txt file2.txt file3.txt

#Checks if the mtar file has been created
if ! [ -e filetar.mtar ]; then
        cd ..
        echo "There has been an error creating the mtar file"
	exit 1
fi

#Create the out directory within the current working directory (i.e., tmp). Create a copy of
#the filetar.mtar file in out.
mkdir out
cp filetar.mtar out

#Switch to the out directory and run mytar to extract the tarball's contents.
cd out
../../mytar -xf filetar.mtar

#Ccompare the contents of each extracted file with the associated
#original file found in the parent directory.
for ((i=1; $i<4; i++))
do
	DIFF=$(diff ../file$i.txt file$i.txt)
	if [ -n "$DIFF" ]; then
        	echo $DIFF
		cd ../..
    		echo "The extracted files are not the same"
		exit 1
	fi
done 

echo "SUCCESS"
cd ../..
exit 0


