/* Assignment 1 of the subject Operating Systems
   Facultad de Informatica
   Universidad Complutense de Madrid. 2015
   @author: Francisco Jose Lozano Serrano
   @author: Juan Carlos Saez Alcaide
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "mytar.h"

extern char *use;
const int DEF_SIZE_OF_BUFFER = 20; //Default number of characters to read from a tar file

/** Copy nBytes bytes from the origin file to the destination file.
 *
 * origin: pointer to the FILE descriptor associated with the origin file
 * destination:  pointer to the FILE descriptor associated with the destination file
 * nBytes: number of bytes to copy
 *
 * Returns the number of bytes actuay copied or -1 if an error occured.
 */
int
copynFile(FILE * origin, FILE * destination, int nBytes)
{
	int i = 0;
	int  c; //Use a int to recognize EOF properly
	while (i < nBytes && (c = getc(origin))!= EOF)
    {
    	 //c is written in the file as a byte
    	if (putc(c,destination) == EOF) return -1;	
		i++;
	}
	return (ferror(origin) || ferror (destination))?-1:i; //Returns -1 if an error ocurred 
}

/** Loads a string from a file.
 *
 * file: pointer to the FILE descriptor 
 * buf: parameter to return the read string. Buf is a
 * string passed by reference. 
 * 
 * The loadstr() function must allocate memory from the heap to store 
 * the contents of the string read from the FILE. 
 * Once the string has been properly "built" in memory, return the starting 
 * address of the string (pointer returned by malloc()) in the buf parameter
 * (*buf)=<address>;
 * 
 * Returns: 0 if success, -1 if error
 */
int
loadstr(FILE * file, char **buf)
{	
	int c;
	int i = 0, sizeBuf;
	sizeBuf = DEF_SIZE_OF_BUFFER + 1;
	if (file == NULL) return -1;//ERROR

	// Allocate memory to contain the whole file:
	*buf = malloc (sizeof(char)* sizeBuf + 1);

	// Copy the file into the buffer:
	while ((c = fgetc(file)) != '\0' && c != EOF)
	{
		(*buf)[i] = (char) c;
		i++;
		//Realloc the buffer for more space
		if (i == sizeBuf-1)
		{
			sizeBuf *= 2;
			*buf = realloc(*buf, sizeBuf);
	   }
	}

	//Set the last character to EOF
	(*buf)[i] = '\0';

	return 0; //SUCCESS
}

/** Read tarball header and store it in memory.
 *
 * tarFile: pointer to the tarball's FILE descriptor 
 * header: output parameter. It is used to return the starting memory address
 * of an array that contains the (name,size) pairs read from the tar file
 * nFiles: output parameter. Used to return the number of files stored in
 * the tarball archive (first 4 bytes of the header)
 *
 * On success it returns EXIT_SUCCESS. Upon failure, EXIT_FAILURE is returned.
 * (both macros are defined in stdlib.h).
 */
int
readHeader(FILE * tarFile, stHeaderEntry ** header, int *nFiles)
{
	stHeaderEntry* array=NULL;
	int nr_files=0,i;

	//Read the number of files (N) from tarfile and store it in nr_files
	fread(&nr_files,sizeof(int),1,tarFile);

	/* Allocate memory for the array */
	if (nr_files <= 0) {
		(*nFiles)=0;
		(*header)=NULL;
		return (EXIT_FAILURE); //ERROR
	}


	array=malloc(sizeof(stHeaderEntry)*nr_files);
	for (i=0; i<nr_files;i++)
	{
		array[i].name = NULL;
		array[i].size = 0;
	} //Initialize the array in case there is an error in the middle of the process

	(*header)=array; //Store the reference before any error occurs

	//Read the (pathname,size) pairs from tarFile and store them in the array
	for (i=0; i<nr_files;i++)
	{
		unsigned int charNum = 0;
		//Read the name of the file
		if (loadstr(tarFile, &array[i].name) == -1) {
			(*nFiles)=i;
			return (EXIT_FAILURE); //ERROR
		}

		//Read the number of characters
		fread (&charNum,sizeof(unsigned int),1, tarFile);
		array[i].size = charNum;
	}

	/* Store the "return values" in the output parameters */
	(*nFiles)=nr_files;
	

	return (EXIT_SUCCESS);
}

/** Creates a tarball archive 
 *
 * nfiles: number of files to be stored in the tarball
 * filenames: array with the path names of the files to be included in the tarball
 * tarname: name of the tarball archive
 * 
 * On success, it returns EXIT_SUCCESS; upon error it returns EXIT_FAILURE. 
 * (macros defined in stdlib.h).
 *
 * HINTS: First reserve room in the file to store the tarball header.
 * Move the file's position indicator to the data section (skip the header)
 * and dump the contents of the source files (one by one) in the tarball archive. 
 * At the same time, build the representation of the tarball header in memory.
 * Finally, rewind the file's position indicator, write the number of files as well as 
 * the (file name,file size) pairs in the tar archive.
 *
 * Important reminder: to calculate the room needed for the header, a simple sizeof 
 * of stHeaderEntry will not work. Bear in mind that, on disk, file names found in (name,size) 
 * pairs occupy strlen(name)+1 bytes.
 *
 */
int
createTar(int nFiles, char *fileNames[], char tarName[])
{	
	int i;
	int headerBytes = sizeof(int) + nFiles*sizeof(unsigned int);


	for (i = 0; i < nFiles; i++)
	{
		headerBytes += (strlen(fileNames[i]) + 1);
	}
    
	FILE *archive = fopen(tarName, "w");
    if (archive == NULL)  return EXIT_FAILURE;  //ERROR
    i = 0;
    fwrite(&i,sizeof(int),1,archive); //Writes 0 at the beginning of the file in case an error occurs


	//Reserve space for the headers
	stHeaderEntry *headers = malloc(nFiles*sizeof(stHeaderEntry));

	//Skip the space reserved for the header
	fseek (archive, headerBytes , SEEK_SET );

	//Fill the information of the header(in memory) and dump the data in the tar file
	FILE *pfile;

	for (i = 0; i < nFiles;i++)
	{
		//Open the file
		pfile = fopen(fileNames[i],"r");
		if (pfile == NULL) {
			free(headers);
			fclose(archive);
			return EXIT_FAILURE;  //ERROR
		}

		/*Write the data to the archive (The maximum number of characters is the maximum
		possible value of an int. This value is stored in the header */
		headers[i].size = copynFile(pfile,archive,INT_MAX);
		headers[i].name = fileNames[i];

		//Close the read file
		fclose(pfile);

		//Write the header information on memory
		if (headers[i].size == -1)
		{
			free(headers);
			fclose(archive);
			return EXIT_FAILURE;  //ERROR
		}
	}

	/* Dump the number of files and the information of the headers
	   at the beginning of the tar file	*/
	fseek(archive, 0, SEEK_SET);
	fwrite(&nFiles,sizeof(int),1,archive);
	for (i = 0; i < nFiles;i++)
	{
		fwrite(headers[i].name, sizeof(char), strlen(headers[i].name) + 1, archive);
		fwrite(&(headers[i].size), sizeof(unsigned int), 1, archive);
	}

	free(headers);
	fclose(archive);

    return EXIT_SUCCESS;
	
	
}

/** Extract files stored in a tarball archive
 *
 * tarName: tarball's pathname
 *
 * On success, it returns EXIT_SUCCESS; upon error it returns EXIT_FAILURE. 
 * (macros defined in stdlib.h).
 *
 * HINTS: First load the tarball's header into memory.
 * After reading the header, the file position indicator will be located at the 
 * tarball's data section. By using information from the 
 * header --number of files and (file name, file size) pairs--, extract files 
 * stored in the data section of the tarball.
 *
 */
int
extractTar(char tarName[])
{

	stHeaderEntry *headers = NULL;
	int nFiles;

	//Opens the file
	FILE *pInput = fopen(tarName,"r");
	if (pInput == NULL) return EXIT_FAILURE;

	//Reads the header and store it in memory
	if (readHeader(pInput, &headers,&nFiles) == EXIT_SUCCESS)
	{
		FILE *pOutput;
		int i,result;
		//Stores the data in a buffer
		for (i=0;i < nFiles; i++)
		{
			//Writes the data in each file
			pOutput = fopen (headers[i].name, "w");
			if (pOutput == NULL) {
				fclose(pInput);
				return EXIT_FAILURE; //ERROR
			}
			result = copynFile (pInput,pOutput,headers[i].size);
			fclose (pOutput);
			if (result == -1) 
			{
				fclose(pInput);
				return EXIT_FAILURE; //ERROR
			}
		}
	}
	fclose(pInput);
	return EXIT_SUCCESS;
}
